package com.example.jhibbele.simplest2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class SimplestActivity extends AppCompatActivity {
    private final static String TAG = "Simplest";
    private Button mButton;
    private Button mMagicButton;
    TextView mMessageTextField;
    public final static String MESSAGE = "com.example.jhibbele.simplest2.message";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simplest);

        mButton = (Button) findViewById(R.id.my_button);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "onClick");
                Toast.makeText(v.getContext(), "click", Toast.LENGTH_SHORT).show();
                Intent intent = SecondActivity.newIntent(v.getContext(), "no magic");
                startActivity(intent);
            }
        });

        mMagicButton = (Button) findViewById(R.id.magic_button);
        mMagicButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "onClick magic");
                Toast.makeText(v.getContext(), "magic", Toast.LENGTH_SHORT).show();
                Intent intent = SecondActivity.newIntent(v.getContext(), "magic");
                startActivity(intent);
            }
        });

    }
}