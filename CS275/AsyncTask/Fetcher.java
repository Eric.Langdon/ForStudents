package com.example.jhibbele.asynctask;

import android.net.Uri;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.example.jhibbele.asynctask.MainActivity.TAG;

public class Fetcher {
    private final static String ADDRESS = "https://jhibbele.w3.uvm.edu";

    public byte[] getUrlBytes(String urlSpec) throws IOException {
        URL url = new URL(urlSpec);
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = connection.getInputStream();
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new IOException(connection.getResponseMessage() +
                        ": with " +
                        urlSpec);
            }
            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            while ((bytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, bytesRead);
            }
            out.close();
            return out.toByteArray();
        } finally {
            connection.disconnect();
        }
    }

    public String getUrlString(String urlSpec) throws IOException {
        return new String(getUrlBytes(urlSpec));
    }

    public Info fetchItems() {
        try {
            String url = Uri.parse(ADDRESS).toString();
            Log.i(TAG, "fetch from " + url);
            String s = getUrlString(url);
            Log.i(TAG, s);
            Info info = new Info(s);
            return info;
        } catch(IOException ioe) {
            Log.e(TAG, "failed to fetch info", ioe);
            return new Info("error");
        }
    }
}
