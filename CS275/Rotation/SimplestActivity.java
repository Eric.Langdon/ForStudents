package com.example.jhibbele.rotation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SimplestActivity extends AppCompatActivity {
    private static final String TAG = "Rotation";
    private static final String KEY_INDEX = "index";

    TextView mTimeField;
    Button mTimeButton;
    String mDateText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simplest);
        Log.d(TAG, "onCreate()");

        mTimeField = (TextView) findViewById(R.id.TimeField);
        mTimeButton = (Button) findViewById(R.id.TimeButton);

        if (savedInstanceState != null) {
            mDateText = savedInstanceState.getString(KEY_INDEX, "");
            mTimeField.setText("current time is: " + mDateText);
        }

        mTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date date = new Date();
                mDateText = new SimpleDateFormat("HH:mm:ss").format(date);
                mTimeField.setText("current time is: " + mDateText);
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString(KEY_INDEX, mDateText);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i(TAG, "onStart()");
    }
    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG, "onStop()");
    }
    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause()");
    }
    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume()");
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy()");
    }
}