#include <stdio.h>
#include <stdlib.h>
#include "slist.jhibbele.h"

void test_two() {
  SListNode *theList;
  int i;
  int fail = 0;

  theList = NULL;

  insertSorted(&theList, 7);
  insertSorted(&theList, 5);
  insertSorted(&theList, 9);
  insertAtEnd(&theList, 9);
  insertSorted(&theList, 5);

  if (isInList(theList, 1)) {
    printf("ERROR: test-two-a don't expect 1 in list\n");
    fail = 1;
  }

  if (! isInList(theList, 5)) {
    printf("ERROR: test-two expect 5 in list\n");
    fail = 1;
  }

  if (! isInList(theList, 9)) {
    printf("ERROR: test-two expect 9 in list\n");
    fail = 1;
  }

  deleteFromList(&theList, 9);

  if (isInList(theList, 9)) {
    printf("ERROR: test-two don't expect 9 in list\n");
    fail = 1;
  }

  for (i=0; i<10; ++i)
    deleteFromList(&theList, i);

  printf("expect empty list\n");
  printList(theList);
}

/*-------------------------------------------------------------------------------*/

void test_three() {
  SListNode *theList;
  int i;

  theList = NULL;

  for (i=1; i<=10; ++i) {
    if (i%2 == 0)
      //insertSorted(&theList, i);
      insertAtEnd(&theList, i);
    else
      //insertSorted(&theList, 10-i);
      insertAtEnd(&theList, 10-i);
  }

  printf("expect this list: 9 2 7 4 5 6 3 8 1 10\n");
  printList(theList);

  for (i=1; i<=10; ++i) {
    if (i%2 == 0)
      deleteFromList(&theList, i);
    else
      deleteFromList(&theList, 10-i);
  }
  printf("expect empty list\n");
  printList(theList);
}

/*------------------------------------------------------------------------*/

void test_four() {
  SListNode *theList;
  int i;

  theList = NULL;

  for (i=1; i<=10; ++i) {
    if (i<5)
      insertSorted(&theList, 1);
    else
      insertSorted(&theList, 10);
  }
  printf("expect this list: 1 1 1 1 10 10 10 10 10 10\n");
  printList(theList);

  deleteFromList(&theList, 1);
  printf("expect this list: 10 10 10 10 10 10\n");
  printList(theList);

  deleteFromList(&theList, 2);
  printf("expect this list: 10 10 10 10 10 10\n");
  printList(theList);

  deleteFromList(&theList, 10);
  printf("expect empty list\n");
  printList(theList);
}

/*------------------------------------------------------------------*/

void test_five() {
  SListNode *theList;
  int i;

  theList = NULL;
  insertSorted(&theList, 5);
  insertSorted(&theList, 1);
  insertSorted(&theList, 9);
  insertSorted(&theList, 3);
  insertSorted(&theList, 8);
  insertSorted(&theList, 4);
  insertSorted(&theList, 6);
  insertSorted(&theList, 2);
  insertSorted(&theList, 7);
  insertSorted(&theList, 1);
  insertSorted(&theList, 5);
  insertSorted(&theList, 9);
  printf("expect this list: 1 1 2 3 4 5 5 6 7 8 9 9\n");
  printList(theList);

  for (i=1; i<10; ++i)
    deleteFromList(&theList, i);
  printf("expect empty list\n");
  printList(theList);
}


/*------------------------------------------------------------------*/

int main(int argc, char *argv[]) {
  test_two();
  test_three();
  test_four();
  test_five();
}
