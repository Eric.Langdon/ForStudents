//
// assignment #1-B
//

#include <stdio.h>
#include <stdlib.h>
#include "dlist.jhibbele.h"

/*----------------------------------------------------------------*/

void printList(DListNode *theList) {
  DListNode *listNode;
  listNode = theList;
  while (listNode != NULL) {
    printf("%d ", listNode->data);
    listNode = listNode->next;
  }
  printf("\n");
}

/*----------------------------------------------------------------*/

void printListReverse(DListNode *theList) {
  DListNode *listNode;
  listNode = theList;

  if (listNode != NULL) {
    if (listNode->next != NULL) {
      while (listNode->next != NULL) {
        listNode = listNode->next;
      }
    }

    while (listNode != NULL) {
      printf("%d ", listNode->data);
      listNode = listNode->prev;
    }
  }

  printf("\n");
}

/*--------------------------------------------------------------------------*/

int insertAtEnd(DListNode **theList, int value) {
  DListNode *listNode;
  DListNode *newNode;

  newNode = (DListNode *) malloc(sizeof(DListNode));
  newNode->prev = NULL;
  newNode->next = NULL;
  newNode->data = value;

  if (*theList == NULL) {
    *theList = newNode;
    return(0);
  } else {
    listNode = *theList;
    while (listNode->next != NULL) {
      listNode = listNode->next;
    }

    listNode->next = newNode;
    newNode->prev = listNode;
    return(0);
  }
}

/*--------------------------------------------------------------------------*/

int insertSorted(DListNode **theList, int value) {
  DListNode *listNode;
  DListNode *newNode;

  newNode = (DListNode *) malloc(sizeof(DListNode));
  newNode->prev = NULL;
  newNode->next = NULL;
  newNode->data = value;

  if (*theList == NULL) {
    // list is empty
    *theList = newNode;
    return(0);
  } else {
    if (value <= (*theList)->data) {
      // insert at beginning (before first node)
      newNode->next = *theList;
      (*theList)->prev = newNode;
      *theList = newNode;
      return(0);
    } else {
      // insert somewhere in the list
      listNode = *theList;
      while (listNode->next != NULL && listNode->data < value) {
        listNode = listNode->next;
      }
      if (value <= listNode->data) {
        listNode->prev->next = newNode;
        newNode->prev = listNode->prev;
        newNode->next = listNode;
        listNode->prev = newNode;
      } else {
        listNode->next = newNode;
        newNode->prev = listNode;
      }
      return(0);
    }
  }
}

/*--------------------------------------------------------------------------*/

int isInList(DListNode *theList, int value) {
  DListNode *listNode;
  int found;

  found = 0;
  listNode = theList;
  while ( ! found && listNode != NULL) {
    if (listNode->data == value)
      found = 1;
    else
      listNode = listNode->next;
  }

  return(found);
}

/*--------------------------------------------------------------------------*/

int deleteFromList(DListNode **theList, int value) {
  DListNode *listNode, *prevNode, *nextNode;
  int found;

  found = 0;
  listNode = *theList;
  prevNode = NULL;
  while (listNode != NULL) {
    nextNode = listNode->next;
    if (listNode->data == value) {
      found = 1;
      if (prevNode != NULL) {
        prevNode->next = nextNode;
        if (nextNode != NULL)
          nextNode->prev = prevNode;
        free(listNode);
      } else {
        *theList = nextNode;
        if (nextNode != NULL)
          nextNode->prev = NULL;
        free(listNode);
      }
    } else {
      prevNode = listNode;
    }
    listNode = nextNode;
  }

  if (found)
    return(0);
  else
    return(1);
}
