int findLoops(DListNode *theList) {
  DListNode *prevNode, *listNode;
  int fail, nodeIdx, i;
  DListNode *nodes[64];

  nodeIdx = 0;
  // check for loop, forward

  prevNode = NULL;

  nodeIdx = 0;
  listNode = theList;
  fail = 0;
  while (nodeIdx < 64 && listNode != NULL && ! fail) {
    nodes[nodeIdx] = listNode;
    ++nodeIdx;
    i = 0;
    while (i < nodeIdx-1 && ! fail) {
      if (listNode == nodes[i]) {
        fail = 1;
        if (prevNode == NULL)
          theList->next = NULL;
        else
          prevNode->next = NULL;
      } else
        ++i;
    }
    if (!fail) {
      prevNode = listNode;
      listNode = listNode->next;
    }
  }
  if (fail)
    printf("ERROR: loop in forward list\n");

  // check for loop, reverse
  prevNode = NULL;
  nodeIdx = 0;
  listNode = theList;
  if (listNode != NULL) {
    while (listNode->next != NULL)
      listNode = listNode->next;
  }

  fail = 0;
  while (nodeIdx < 64 && listNode != NULL && ! fail) {
    nodes[nodeIdx] = listNode;
    ++nodeIdx;
    i = 0;
    while (i < nodeIdx-1 && ! fail) {
      if (listNode == nodes[i]) {
        fail = 1;
        if (prevNode == NULL)
          theList->prev = NULL;
        else
          prevNode->prev = NULL;
      } else
        ++i;
    }
    if (!fail) {
      prevNode = listNode;
      listNode = listNode->prev;
    }
  }
  if (fail)
    printf("ERROR: loop in reverse list\n");
  return(fail);
}

//---------------------------------------------------------------------

int checkReverse(DListNode *theList) {
  DListNode *nodes[64];
  int idx, fail;
  DListNode *listNode, *prevNode;

  idx = 0;
  listNode = theList;
  if (listNode == NULL)
    return(0);

  if (listNode->next == NULL) {
    if (listNode->prev != NULL) {
      printf("ERROR: prev pointer is wrong\n");
      return(1);
    }
  }

  prevNode = NULL;
  while (listNode->next != NULL) {
    nodes[idx] = listNode;
    ++idx;
    listNode = listNode->next;
  }

  fail = 0;
  idx = idx - 1;
  listNode = listNode->prev;
  while (! fail && idx >= 0 && listNode != NULL) {
    if (listNode != nodes[idx]) {
      printf("ERROR: prev pointer is wrong\n");
      fail = 1;
    }
    listNode = listNode->prev;
    idx = idx - 1;
  }


  if (idx != -1) {
    printf("ERROR: reverse list is not correct length: %d\n", idx);
    fail = 1;
  }

  return(fail);
}
