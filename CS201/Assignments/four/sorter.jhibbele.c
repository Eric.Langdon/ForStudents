#include <pthread.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define NUMVALS 1024

void *sorter(void *param);
void *merger(void *param);

typedef struct {
  int start_index;
  int end_index;
} SortInfo;

int *Array;
int *SortedArray;

int genRand(int low, int high) {
  // generate a random integer r such that low <= r <= high
  double r1, r2;
  int rtnval;
  r1 = drand48();
  r2 = (1 + high - low) * r1;
  rtnval = low + floor(r2);
  return(rtnval);
}

//--------------------------------------------------------------------
 
int main(int argc, char *argv[]) {
  SortInfo sortInfo1, sortInfo2;
  pthread_t tid1, tid2, tid3;
  pthread_attr_t attr;
  int i, secondStart;

  Array = (int *) malloc(NUMVALS * sizeof(int));
  SortedArray = (int *) malloc(NUMVALS * sizeof(int));

  for (i=0; i<NUMVALS; ++i)
    Array[i] = genRand(0, 250);
  
  /* get default thread attributes */
  pthread_attr_init(&attr);

  sortInfo1.start_index = 0;
  sortInfo1.end_index = NUMVALS/2;
  sortInfo2.start_index = sortInfo1.end_index + 1;
  sortInfo2.end_index = NUMVALS - 1;

  // create the threads
  pthread_create(&tid1, &attr, sorter, &sortInfo1);
  pthread_create(&tid2, &attr, sorter, &sortInfo2);

  // wait for the threads to terminate 
  pthread_join(tid1, NULL);
  pthread_join(tid2, NULL);

//for (i=0; i<NUMVALS; ++i)
//  printf("%d ", Array[i]);
//printf("\n");

  // create the merger thread
  secondStart = sortInfo2.start_index;
  pthread_create(&tid3, &attr, merger, &secondStart);
  pthread_join(tid3, NULL);

  for (i=0; i<NUMVALS; ++i)
    printf("%d ", SortedArray[i]);
  printf("\n");
}

//--------------------------------------------------------------------

void *sorter(void *data) {
  SortInfo *sortInfo;
  int i, j, swap;

  sortInfo = (SortInfo *) data;

  for (i=sortInfo->start_index; i<=sortInfo->end_index; ++i) {
    for (j=i+1; j<=sortInfo->end_index; ++j) {
      if (Array[i] > Array[j]) {
        swap = Array[i];
        Array[i] = Array[j];
        Array[j] = swap;
      }
    }
  }

  pthread_exit(0);
}

//--------------------------------------------------------------------

void *merger(void *data) {
  int *secondStart;
  int idx1, idx2;
  int mergeIdx;

  secondStart = (int *) data;
  mergeIdx = 0;
  idx1 = 0;
  idx2 = *secondStart;

  while (idx1 < *secondStart && idx2 < NUMVALS) {
    printf("Array[%d] = %d   Array[%d] = %d\n", idx1, Array[idx1], idx2, Array[idx2]);
    if (Array[idx1] < Array[idx2]) {
      SortedArray[mergeIdx] = Array[idx1];
      ++idx1;
      ++mergeIdx;
    } else if (Array[idx2] < Array[idx1]) {
      SortedArray[mergeIdx] = Array[idx2];
      ++idx2;
      ++mergeIdx;
    } else {
      SortedArray[mergeIdx] = Array[idx1];
      ++mergeIdx;
      SortedArray[mergeIdx] = Array[idx2];
      ++mergeIdx;
      ++idx1;
      ++idx2;
    }
  }

  while (idx1 < *secondStart) {
    SortedArray[mergeIdx] = Array[idx1];
    ++idx1;
    ++mergeIdx;
  }

  while (idx2 < NUMVALS) {
    SortedArray[mergeIdx] = Array[idx2];
    ++idx2;
    ++mergeIdx;
  }

  pthread_exit(0);
}
