public class Pawn {
  public String color;
  int ID;
  int position;
  
  public Pawn() {
    ID = -1;
    position = 0;
  }
  
  public Pawn(int pawnID, String color) {
    this.ID = pawnID;
    this.color = color;
    this.position = 0;
  }
  
  public int getID() {
    return this.ID;
  }
  
  public void moveTo(int tileNumber) {
    this.position = tileNumber;
  }
  
  public int getPosition() {
    return this.position;
  }
  
  public String getColor() {
    return this.color;
  }
}