import javafx.scene.image.Image;
import java.util.ArrayList;

public class Board {
  private static Board sBoard;
  private ArrayList<Pawn> pawns;
  
  public static Board get() {
    if (sBoard == null) {
      sBoard = new Board();
    }
    return sBoard;
  }
  
  private Board() {
    System.out.println("hello from Board()");
  }
  
  public void Initialize() {
    System.out.println("hello from Board.Initialize()");
    pawns = new ArrayList<> ();
  }

  public void addPawn(int pawnID, String color) {
    pawns.add(new Pawn(pawnID, color));
    System.out.println("add pawn " + pawnID + " " + color);
  }
  
  public Pawn getPawn(int pawnID) {
    int i = 0;
    while ( i < pawns.size() ) {
      if (pawns.get(i).getID() == pawnID)
        return pawns.get(i);
      else
        i = i + 1;
    }
    return null;
  }
  
  public boolean movePawnTo(int pawnID, int tileNumber) {
    Pawn pawn = getPawn(pawnID);
    if (pawn != null) {
      pawn.moveTo(tileNumber);
      return true;
    } else {
      return false;
    }
  }
  
  public boolean isSpaceFree(int tileNumber) {
    int i = 0;
    while ( i < pawns.size() ) {
      if (pawns.get(i).getPosition() == tileNumber)
        return false;
      ++i;
    }
    return true;
  }
}