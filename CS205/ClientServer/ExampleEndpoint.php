<?php 

/**
 * Let's assume this file is at https://mywebsite.com/api/ExampleEndpoint.php 
 *
 * Any request (POST, GET, PUT, PATCH, DELETE) made to the above url will hit 
 * this file. I'm not sure of the setup and support for PUT/PATCH/DELETE on 
 * w3.uvm.edu, but POST and GET and pretty good bets. 
 *
 * Before going any further recognize you could litterally just write...
 *
 * echo "You hit the endpoint with a GET request.";
 * die();
 *
 * ... and have yourself an endpoint. It's not any more complicated than that.
 * HOWEVER, you'll most likely want to do more, and at least try and implement 
 * some sort of authorization so you don't have unentended consequences from 
 * people and robots hitting this file. 
 *
 * You can decide to send some credentials as parameters (bad idea) or use and 
 * Authorization headers. 
 *
 * IF you are using Authorization headers AND are hosting it on w3.uvm.edu, 
 * you will need to create/edit an .htaccess file in the root of your project
 * to allow for this. It is not default out of the box on that server.
 *
 * IMPORTANT: The following has a million missing pieces and is incredibly 
 * simplified. With that said, the flow of handling a response may 
 * resemble something like this (assuming GET request)...
 *
 * authored by Ryan Berliner, 10/2018
 */

// Step 1: Initialize response.
$response = new ExampleResponseClass(); 

// Step 2. Check credentials in parameters or authorization header.
if ( $_GET['token'] !== SECRET_TOKEN ) {
  // Remember how the response defaulted to failure? Let's just change the 
  // message and send it back. Code execution will stop after sending the failed
  // response to the client.
  $response->set_message( ' Failed authentication.' );
  $response->send_response();
}

// Step 3. Process request.
 // Doesn't need to be a string, can be other objects too. Query the database 
 // for it, hard code it, do whatever.
$my_data = 'The data I want!';


// Step 4. Fill in response and return it.
$response->set_success( true );
$response->set_data( $my_data );
$response->send_response();

